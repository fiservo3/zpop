import numpy as np
import matplotlib.pyplot as plt

U = 10
Rz = 10e3

expe = np.loadtxt("data1.txt")
n = expe.shape[0]

print("loaded rows: " + str(n))

data = np.ndarray((n, 3))
data[:,:2] = expe


for row in data:
	row[2]= (U - row[1])*(Rz)/row[1]



plt.scatter(data[:,0], data[:,2])
plt.ylabel("R_f [\Omega]")
plt.xlabel("Opticky vykon [nevim]")
plt.loglog()
plt.show()