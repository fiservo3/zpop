import numpy as np
import matplotlib.pyplot as plt


expe = np.loadtxt("data1.txt")
n = expe.shape[0]

print("loaded rows: " + str(n))



plt.plot(expe[:,0], expe[:,1], 'b')
plt.plot(expe[:,0], expe[:,1], 'bs', label="P = A")
plt.plot (expe[:,0], expe[:,2], 'r')
plt.plot (expe[:,0], expe[:,2], 'r^', label="P = B")
plt.plot (expe[:,0], expe[:,3], 'g')
plt.plot (expe[:,0], expe[:,3], 'go', label="P = C")
plt.ylabel("I [A]")
plt.xlabel("U [V]")
plt.legend()
plt.show()