import numpy as np
import matplotlib.pyplot as plt

expe = np.loadtxt("data1.txt")
n = expe.shape[0]

print("loaded rows: " + str(n))

data = np.ndarray(())

x = np.arange(0, 5, 0.1)
y = np.sin(x)
plt.scatter(x, y)
plt.show()