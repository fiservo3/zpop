import numpy as np
from scipy.fftpack import fft


# Time lengthof sample - 100 us

L=0.0001

# Number of sample points
N = 2000

# sample spacing
T = L / N
x = np.linspace(0.0, N*T, N)

y5 = np.zeros_like(x)
y10 = np.zeros_like(x)
y15 = np.zeros_like(x)
#add pulse here
#5
y5[1000-50:1000+50] = 1
y10[1000-100:1000+100] = 1
y15[1000-150:1000+150] = 1

yf5 = fft(y5)
yf10 = fft(y10)
yf15 = fft(y15)
xf= np.linspace(0.0, 1.0/(2.0*T), N//2)


import matplotlib.pyplot as plt


plt.subplot(2, 1, 1)
plt.plot(x, y5,  label="5ms")
plt.plot(x, y10, label="10ms")
plt.plot(x, y15, label="15ms")
plt.legend()
plt.grid()

plt.title('Pulzy v časové doméně')
plt.xlabel('Čas [s]')
plt.ylabel('I')
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))


plt.subplot(2, 1, 2)
plt.title('Pulzy ve frekvenční doméně')
plt.plot(xf, 2.0/N * np.abs(yf5[0:N//2]) , label="5ms")
plt.plot(xf, 2.0/N * np.abs(yf10[0:N//2]), label="10ms")
plt.plot(xf, 2.0/N * np.abs(yf15[0:N//2]), label="15ms")
plt.xlim(0, 1e6)

plt.ylabel('I')
plt.xlabel('f [Hz]')
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))


plt.subplots_adjust( hspace=0.5)
plt.grid()
plt.legend()
plt.show()