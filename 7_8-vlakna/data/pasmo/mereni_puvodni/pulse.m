function pulse(varargin);

% Funkce pulse.m provadi mereni prenosove funkce z prenosu spektralne sirokeho impulzu. Pro vycteni
% hodnot z osciloskopu Tektronix vyuziva funkci scope.m ve formatu [osa_x,osa_y] = scope(kanal). Po
% spusteni programu nasledujte instrukce na obrazovce. Program prepoklada pripojeni fotodetektoru k
% prvnimu kanalu osciloskopu Nejdrive probehne mereni s kratkym kalibracnim vlaknem a posleze mereni
% s dlouhym vlaknem. Program automaticky uklada vysledky do aktualniho adresare v ruznych formatech
% s prefixem, ktery je potrebne zadat na zacatku mereni.  Zpracovani mereni probiha automaticky,
% jsou analyzovany namerene prubehy, zpocteny prislusne Fourierovy transformace a nakonec i vysledna
% prenosova funkce.
%
% Pokud je funkce spustena bez parametru, probehne realne mereni. Pokud je spustena s libovolnym
% parametrem, probehne zpracovani dat z jiz hotoveho mereni, ktereho vysledky byly ulozeny v souboru
% s danym prefixem.

close all;clc;
pocet_vzorku = 1000000; % volba rozmeru pracovniho signalu
prefix = input('Zadejte prefix pro jmena vstupnich/vystupnich souboru: ','s');

if isempty(varargin) % v pripade spusteni bez parametru provede mereni 

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%% kalibracni mereni s kratkym vlaknem %%%%%%%%%%%%%%%%

	clc;
	disp('Nastavte na vstupu obdelnikovy puls pozadovane delky. Pro pokracovani stisknete libovolnou klavesu.');
	pause; 

	clc;
	delka_pulzu = input('Zadejte delku pulzu v mikrosekundach: ');

	clc;
	disp('Po ustaleni prubehu pozastavte prubeh osciloskopu. Pro vycteni prubehu stisknete libovolnou klavesu.')
	pause;

	clc;
	disp('Vycitam prubeh signalu na detektoru ...');
	[casova_osa_detektor_kalibrace,signal_detektor_kalibrace] = scope(1);
	disp('Vycteni OK!');

	%%%%% konec kalibracniho mereni %%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%% mereni s dlouhym vlaknem %%%%%%%%%%%%%%%%%%%%%%%%%%%

	clc;
	disp('Vymente kalibracni (kratke) vlakno za merene (dlouhe) a po ustaleni prubehu pozastavte osciloskop. Vycteni probehne po stisknuti libovolne klavesy.');  
	pause;

	clc;
	disp('Vycitam prubeh signalu na detektoru ...');
	[casova_osa_detektor_mereni,signal_detektor_mereni] = scope(1);
	disp('Vycteni OK!');

	%%%%% konec mereni %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%% ulozeni namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	save([prefix,'_namerena_data_casova_domena']);

	%%%%% konec ukladani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

else	% v pripade spusteni s parametrem nacte namerena data za souboru na disku

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%% nacteni namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	load([prefix,'_namerena_data_casova_domena']);

	%%%%% konec nacitani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% zpracovani namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%

% urceni poctu namerenych vzorku
delka_signalu = max(size(signal_detektor_mereni));

% nastaveni minimalni hodnoty signalu na 0
signal_detektor_kalibrace = signal_detektor_kalibrace - min(signal_detektor_kalibrace);
signal_detektor_mereni = signal_detektor_mereni - min(signal_detektor_mereni);

% umisteni namerenych dat do vektoru nul - dulezite pro zjemneni vzorkovani ve frekvencni rovine
signal_kalibrace = zeros(pocet_vzorku,1);
signal_kalibrace(pocet_vzorku/2-delka_signalu/2:pocet_vzorku/2+delka_signalu/2-1) = signal_detektor_kalibrace;
signal_mereni = zeros(pocet_vzorku,1);
signal_mereni(pocet_vzorku/2-delka_signalu/2:pocet_vzorku/2+delka_signalu/2-1) = signal_detektor_mereni;

% Fourierova transformace signalu
spektrum_kalibrace = fft(fftshift(signal_kalibrace));
spektrum_mereni = fft(fftshift(signal_mereni));


% urceni sirky pasma, ve kterem lze pri dane delce pulzu pocitat z mereni prenosovou funkci
casovy_krok = max(casova_osa_detektor_mereni)/delka_signalu;
delka_pulzu = 15
frekvencni_krok = 1/(casovy_krok*pocet_vzorku);
hranice_pasma = round(1/(delka_pulzu*1e-6*frekvencni_krok));

% spocteni prenosove funkce
prenosova_funkce_vlakna = spektrum_mereni(1:hranice_pasma)./spektrum_kalibrace(1:hranice_pasma);

%%%%% konec zpracovani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% vykresleni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1);
set(gcf,'Position',[1 1 1000 700]);

subplot(2,2,1);
plot([1:delka_signalu]*casovy_krok*1e6,signal_detektor_kalibrace*1e3,'b');hold on;
plot([1:delka_signalu]*casovy_krok*1e6,signal_detektor_mereni*1e3,'r');hold off;
axis([0 delka_signalu*casovy_krok*1e6 0 1e3*1.1*max(max(signal_detektor_kalibrace,signal_detektor_mereni))]);
title('Casovy prubeh pulzu pri kalibraci a mereni');
xlabel('cas [us]');
ylabel('signal na detektoru [mV]');
legend('kalibrace (kratke vlakno)','mereni (dlouhe vlakno)');

subplot(2,2,2);
plot([1:hranice_pasma]*frekvencni_krok*1e-3,abs(spektrum_kalibrace(1:hranice_pasma)),'b');hold on;
plot([1:hranice_pasma]*frekvencni_krok*1e-3,abs(spektrum_mereni(1:hranice_pasma)),'r');hold off;
axis([0 hranice_pasma*frekvencni_krok*1e-3 0 1.1*max(max(abs(spektrum_kalibrace),abs(spektrum_mereni)))]);
title('Prubeh amplitudy spektra prenasenych pulzu');
xlabel('frekvence [kHz]');
ylabel('a.u.');
legend('kalibrace (kratke vlakno)','mereni (dlouhe vlakno)');


subplot(2,2,3);
plot([1:hranice_pasma]*frekvencni_krok*1e-3,abs(prenosova_funkce_vlakna));
axis([0 hranice_pasma*frekvencni_krok*1e-3 0 1.1*max(abs(prenosova_funkce_vlakna))])
title('Amplituda prenosove funkce vlakna');
xlabel('frekvence [kHz]');
ylabel('amplituda prenosove funkce');

subplot(2,2,4);
plot([1:hranice_pasma]*frekvencni_krok*1e-3,angle(prenosova_funkce_vlakna));
axis([0 hranice_pasma*frekvencni_krok*1e-3 -pi pi])
title('Faze prenosove funkce vlakna');
xlabel('frekvence [kHz]');
ylabel('faze prenosove funkce');

%%%%% konec vykresleni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% ulozeni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print('-dpng','-f1','-r1200',[prefix,'_grafy_mereni_v_casove_domene.png']);
print('-depsc2','-f1',[prefix,'_grafy_mereni_v_casove_domene.eps']);

%%%%% konec programu %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
