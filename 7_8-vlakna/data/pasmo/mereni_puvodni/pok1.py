import scipy.io
import matplotlib.pyplot as plt
loadmat = scipy.io.loadmat
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optimize
import math

def exponenciala(x, a, b, c, d):
	return a*np.exp(b*x+c) +d



prenos_kalibrace = (loadmat("freq_namerena_data_frekvencni_domena.mat")["prenos_kalibrace"])
prenos_mereni = (loadmat("freq_namerena_data_frekvencni_domena.mat")["prenos_mereni"])

param_kalibrace = np.polyfit(*prenos_kalibrace, 7)
param_mereni = np.polyfit(*prenos_mereni, 7)


kalib = np.poly1d(param_kalibrace)
meren = np.poly1d(param_mereni)

prenos = np.polydiv(meren, kalib)
prenos = prenos[0]+prenos[1]

print(prenos)

pren = lambda x: 10* np.log(meren(x)/kalib(x))
pos_pren = lambda x: 3+ pren(x) - pren(0)
sirka = optimize.brentq(pos_pren, 0, 100000)

print("sirka pasma:" + str(sirka))

x_fity = np.linspace(0, 100010, 1000)


graf_kalib = x_fity, kalib(x_fity)
graf_mereni = x_fity, meren(x_fity)
graf_prenos = x_fity, meren(x_fity)/kalib(x_fity)

#print(graf_prenos[1])
#graf_prenos[1] = 10* np.log(graf_prenos[:,1])
graf_prenos_log = x_fity, 10* np.log(graf_prenos[1])

plt.subplot(121)
plt.plot(*prenos_kalibrace, "bx", label = "kalibrační vlákno")
plt.plot(*prenos_mereni, "g*", label = "měřené vlákno")
plt.plot(*graf_kalib, "b-" )
plt.plot(*graf_mereni, "g-")

plt.xlabel("f [Hz]")
plt.ylabel("U [mV]")

plt.legend()
plt.subplot(122)

plt.plot(x_fity, pren(x_fity), "orange", label = "spočtený přenos")

plt.xlabel("f [Hz]")
plt.ylabel("A [dB]")

ax=plt.subplot(122)

ax.arrow(sirka, pren(0), 0, -3, fc="b", ec="b", length_includes_head = True, label="3 dB pokles")


plt.legend()
plt.show()


