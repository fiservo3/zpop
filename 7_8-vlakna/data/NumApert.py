import numpy as np
import matplotlib.pyplot as plt


daleko = np.loadtxt("NumApert1.txt")
blizko = np.loadtxt("NumApert2.txt")
n = daleko.shape[0]

print("loaded rows: " + str(n))



plt.plot(daleko[:,0], daleko[:,1], 'g')
plt.plot(daleko[:,0], daleko[:,1], 'gs', label="P = A")

plt.plot(blizko[:,0], blizko[:,1], 'b')
plt.plot(blizko[:,0], blizko[:,1], 'bs', label="P = A")

plt.ylabel("p [mW]")
plt.xlabel("uhel [°]")
plt.legend()
plt.show()