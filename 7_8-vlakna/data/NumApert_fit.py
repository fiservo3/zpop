import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optimize
import math

daleko = np.loadtxt("NumApert1.txt")
blizko = np.loadtxt("NumApert2.txt")

n = daleko.shape[0]
k = blizko.shape[0]

print("blizko: loaded rows: " + str(k))
print("daleko: loaded rows: " + str(n))

def gauss (x, u, s, a):
	return a*(np.exp(-((x-u)**2)/(2*s**2)))

dolni = (0.05, 0.3)
peak = (0.3, 0.7)
horni = (0.7, 1)

def part (data, od, do):
	n=data.shape[0]
	return (data[round(n*od):round(n*do)])

#def part2d (data, od, do)
x_fity = np.linspace(blizko.min(), blizko.max(), 1000)


plt.plot(daleko[:,0], daleko[:,1], 'gs', label="daleko")
plt.plot(blizko[:,0], blizko[:,1], 'bx', label="blizko")


param_blizko, sp = optimize.curve_fit(gauss, blizko[:,0], blizko[:,1], p0=(-.5, 1, 0.05), maxfev=100)
param_daleko, sp = optimize.curve_fit(gauss, daleko[:,0], daleko[:,1], p0=(-.5, 1, 0.05), maxfev=100)


plt.plot(x_fity, gauss(x_fity,*param_blizko), "b")
plt.plot(x_fity, gauss(x_fity,*param_daleko), "g")

plt.ylabel("p [mW]")
plt.xlabel("uhel [°]")
plt.legend()
plt.show()