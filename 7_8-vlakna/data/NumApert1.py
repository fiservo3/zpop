import numpy as np
import matplotlib.pyplot as plt


expe = np.loadtxt("NumApert1.txt")
n = expe.shape[0]

print("loaded rows: " + str(n))



plt.plot(expe[:,0], expe[:,1], 'b')
plt.plot(expe[:,0], expe[:,1], 'bs', label="P = A")

plt.ylabel("p [mW]")
plt.xlabel("uhel [°]")
plt.legend()
plt.show()