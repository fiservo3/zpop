function pk2pk = peak2peak(channel);

s = serial('/dev/ttyUSB0');

set(s,'BaudRate',9600);
set(s,'InputBufferSize',50000);

fopen(s);

fprintf(s,['MEASUrement:IMMed:SOUrce CH',num2str(channel)],'async');
pause(0.5);
fprintf(s,'MEASUrement:IMMed:TYPe PK2pk','async');
pause(0.5);
fprintf(s,'MEASUrement:IMMed:VALue?','async');
pause(0.5);
p2p = fscanf(s);

fclose(s);
delete(s);
clear s

pk2pk = str2num(p2p(findstr(p2p,' ')+1:max(size(p2p))));
