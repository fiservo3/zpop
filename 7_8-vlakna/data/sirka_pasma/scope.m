function [time,dat] = scope(ch)

channel = num2str(ch);

s = serial('/dev/ttyUSB0');

set(s,'BaudRate',9600);
set(s,'InputBufferSize',50000);

fopen(s);

fprintf(s,sprintf('%s%s','DATa:SOUrce CH',channel),'async');
pause(0.5);
fprintf(s,'DATa:ENCdg ASCII','async');
pause(0.5);
fprintf(s,'DATa:WIDth 1','async');
pause(0.5);
fprintf(s,'DATa:STARt 1','async');
pause(0.5);
fprintf(s,'DATa:STOP 2500','async');
pause(0.5);
fprintf(s,sprintf('%s%s%s','CH',channel,':POSition?'),'async');
pause(0.5);
chp = fscanf(s);
fprintf(s,sprintf('%s%s%s','CH',channel,':SCALe?'),'async');
pause(0.5);
chs = fscanf(s);
fprintf(s,'HOR:MAIn:SCALe?','async');
pause(0.5);
hms = fscanf(s);
fprintf(s,'HOR:MAIn:POSition?','async');
pause(0.5);
hmp = fscanf(s);
fprintf(s,'CURVe?','async');
pause(8);
curve = fscanf(s);

fclose(s);
delete(s);
clear s

nchp = str2num(chp(findstr(chp,' ')+1:max(size(chp))));
nchs = str2num(chs(findstr(chs,' ')+1:max(size(chs))));
nhms = str2num(hms(findstr(hms,' ')+1:max(size(hms))));
nhmp = str2num(hmp(findstr(hmp,' ')+1:max(size(hmp))));

pomoc = curve(8:max(size(curve)));
mez = findstr(pomoc,',');
for j=1:max(size(mez))
     pomoc(mez(j)) = ' ';
end;  
c = sscanf(pomoc,'%d');

dat = c/25*nchs -nchs*nchp;
time = (0:2499)*nhms/250 -5*nhms+nhmp;

