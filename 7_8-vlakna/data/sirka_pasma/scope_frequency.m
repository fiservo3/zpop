function frequency = scope_frequency(channel);

s = serial('/dev/ttyUSB0');

set(s,'BaudRate',9600);
set(s,'InputBufferSize',50000);

fopen(s);

fprintf(s,['MEASUrement:IMMed:SOUrce CH',num2str(channel)],'async');
pause(0.5);
fprintf(s,'MEASUrement:IMMed:TYPe FREQuency','async');
pause(0.5);
fprintf(s,'MEASUrement:IMMed:VALue?','async');
pause(0.5);
signal_frequency = fscanf(s);

fclose(s);
delete(s);
clear s

frequency = str2num(signal_frequency(findstr(signal_frequency,' ')+1:max(size(signal_frequency))));
