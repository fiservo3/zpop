function harmonic(varargin);

% Funkce harmonic.m provadi mereni prenosove funkce ve frekvencni domene. Pro vycteni dat z
% osciloskopu Tektronix vyuziva funkce scope_frequency.m a peak2peak.m ve formatu frekvence =
% scope_frequency(kanal) a modulace = peak2peak(kanal). Program predpoklada pripojeni fotodetektoru
% k prvnimu kanalu osciloskopu. Nejdrive probehne mereni s kratkym kalibracnim vlaknem a posleze
% mereni s dlouhym vlaknem. Program automaticky uklada vysledky do aktualniho adresare v ruznych
% formatech s prefixem, ktery je potrebne zadat na zacatku mereni.  Zpracovani mereni probiha
% automaticky, namerene prenosove funkce jsou fitovany polynomem, aby bylo mozne s nimi operovat i v
% pripade, ze kalibrace byla provedena pro jine frekvence nez samotne mereni.
%
% Pokud je funkce spustena bez parametru, probehne realne mereni. Pokud je spustena s libovolnym
% parametrem, probehne zpracovani dat z jiz hotoveho mereni, ktereho vysledky byly ulozeny v souboru
% s danym prefixem.

close all;clc;
prefix = input('Zadejte prefix pro jmena vstupnich/vystupnich souboru: ','s');

if isempty(varargin) % v pripade spusteni bez parametru provede mereni

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% kalibracni mereni s kratkym vlaknem %%%%%%%%%%%%%%%%

	prenos_kalibrace = zeros(2,1);
	prenos_mereni = zeros(2,1);
	k = 0;
	dalsi_mereni = 'a';

	clc;
	disp('Sestavte schema s kalibracnim vlaknem a stisknete libovolnou klavesu ...');
	pause;

	while dalsi_mereni == 'a'
	
		k = k + 1;

		clc;
		disp('Nastavte harmonicky signal pozadovane frekvence na generatoru a vyladte signal z detektoru.');
		disp('Pro zahajeni mereni stisknete libovolnou klavesu ...');
		pause;

		% vycteni frekvence a modulace signalu z detektoru na prvnim kanalu osciloskopu
		clc;
		disp('Vycitam frekvenci signalu ...');
		frequency = scope_frequency(1)
		disp('Vycitam signal z detektoru ...');
		p2p = peak2peak(1)
	 	disp('Probiha zpracovani mereni ...');
		prenos_kalibrace(1,k) = frequency;
		prenos_kalibrace(2,k) = p2p;
		
		dalsi_mereni= input('Provest dalsi mereni? [a/n]: ','s');
	
	end

	%%%%% konec kalibracniho mereni %%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% mereni s dlouhym vlaknem %%%%%%%%%%%%%%%%%%%%%%%%%%%	

	clc;
	disp('Sestavte schema s merenym vlaknem a stisknete libovolnou klavesu ...');
	pause;
	k = 0;
	dalsi_mereni = 'a';
	
	while dalsi_mereni == 'a'
		
		k = k + 1;
		
		clc;
		disp('Nastavte harmonicky signal pozadovane frekvence na generatoru a vyladte signal z detektoru.');
		disp('Pro zahajeni mereni stisknete libovolnou klavesu ...');
		pause;

		% vycteni frekvence a modulace signalu z detektoru na prvnim kanalu osciloskopu
		clc;
		disp('Vycitam frekvenci signalu ...');
		frequency = scope_frequency(1)
		disp('Vycitam signal z detektoru ...');
		p2p = peak2peak(1)
	 	disp('Probiha zpracovani mereni ...');
		prenos_mereni(1,k) = frequency;
		prenos_mereni(2,k) = p2p;
		
		dalsi_mereni= input('Provest dalsi mereni? [a/n]: ','s');
	
	end
 
        %%%%% konec mereni %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% ulozeni namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        save([prefix,'_namerena_data_frekvencni_domena']);

        %%%%% konec ukladani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

else    % v pripade spusteni s parametrem nacte namerena data za souboru na disku

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% nacteni namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        load([prefix,'_namerena_data_frekvencni_domena']);

        %%%%% konec nacitani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
end
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% zpracovani namerenych dat %%%%%%%%%%%%%%%%%%%%%%%%%%

% fitovani prenosove funkce polynomem N-1 stupne, kde N je pocet namerenych hodnot
[prenos_kalibrace_fit,s,mu1] = polyfit(prenos_kalibrace(1,:),prenos_kalibrace(2,:),size(prenos_kalibrace,2)-1);
[prenos_mereni_fit,s,mu2] = polyfit(prenos_mereni(1,:),prenos_mereni(2,:),size(prenos_mereni,2)-1);

% urceni frekvencniho rozsahu, ve kterem lze pocitat prenosovou funkci
merene_pasmo = min(max(prenos_kalibrace(1,:)),max(prenos_mereni(1,:)));

% vypocet prenosove funkce v danem pasmu
prenosova_funkce = polyval(prenos_mereni_fit,[0:merene_pasmo/1000:merene_pasmo],[],mu2)./polyval(prenos_kalibrace_fit,[0:merene_pasmo/1000:merene_pasmo],[],mu1);

%%%%% konec zpracovani dat %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% vykresleni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(1);
set(gcf,'Position',[1 1 1000 400]);

subplot(1,2,1);
plot(prenos_kalibrace(1,:)*1e-3,1e3*prenos_kalibrace(2,:),'bo');hold on;
plot(prenos_mereni(1,:)*1e-3,1e3*prenos_mereni(2,:),'ro');
plot([0:merene_pasmo/1000:merene_pasmo]*1e-3,1e3*polyval(prenos_kalibrace_fit,[0:merene_pasmo/1000:merene_pasmo],[],mu2),'b');
plot([0:merene_pasmo/1000:merene_pasmo]*1e-3,1e3*polyval(prenos_mereni_fit,[0:merene_pasmo/1000:merene_pasmo],[],mu2),'r');
axis([0 merene_pasmo*1e-3 0 1e3*1.1*max(max(prenos_kalibrace(2,:)),max(prenos_mereni(2,:)))]);
title('Namerene hodnoty prenosu pri kalibraci a mereni');
xlabel('frekvence [kHz]');
ylabel('amplituda preneseneho signalu [mV]');
legend('kalibrace - namerena','mereni - namerena','kalibrace - fitovana','mereni - fitovana');

subplot(1,2,2);
plot([0:merene_pasmo/1000:merene_pasmo]*1e-3,prenosova_funkce);
axis([0 merene_pasmo*1e-3 0 1.1*max(prenosova_funkce)]);
title('Amplituda prenosove funkce vlakna');
xlabel('frekvence [kHz]');
ylabel('amplituda prenosove funkce');

%%%%% konec vykresleni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% ulozeni vysledku %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print('-dpng','-f1','-r1200',[prefix,'_grafy_mereni_ve_frekvencni_domene.png']);
print('-depsc2','-f1',[prefix,'_grafy_mereni_ve_frekvencni_domene.eps']);

%%%%% konec programu %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
