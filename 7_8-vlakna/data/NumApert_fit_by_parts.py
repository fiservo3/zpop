import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optimize
import math
from matplotlib.widgets import Cursor

daleko = np.loadtxt("NumApert1.txt")
blizko = np.loadtxt("NumApert2.txt")

n = daleko.shape[0]
k = blizko.shape[0]

print("blizko: loaded rows: " + str(k))
print("daleko: loaded rows: " + str(n))

def gauss (x, u, s, a, b):
	return a*(np.power(b, -((x-u)**2)/(2*s**2)))

dolni = (0, 0.3)
peak = (0.4, 0.6)
horni = (0.7, 1)

def part (data, od, do):
	n=data.shape[0]
	return (data[round(n*od):round(n*do)])

def border (data, od, do):
	n=data.shape[0]

	if (do == 1):
		print("Zde dochazi k ponekud nepresnemu posunuti, ale to je mi celkem burt")
		return (data[round(n*od)], data[round(n*do-1)])
	return (data[round(n*od)], data[round(n*do)])

#def part2d (data, od, do)
x_fity = np.linspace(blizko.min(), blizko.max(), 1000)


plt.plot(daleko[:,0], daleko[:,1], 'gs', label="daleko")

plt.plot(blizko[:,0], blizko[:,1], 'bx', label="blizko")


kraje_blizko =np.concatenate([part(blizko, *dolni),part(blizko, *horni)]  , axis=0)
kraje_daleko =np.concatenate([part(daleko, *dolni),part(daleko, *horni)]  , axis=0)


param_blizko_bot, sp = optimize.curve_fit(gauss, *kraje_blizko.T, maxfev=10000)
param_blizko_peak, sp = optimize.curve_fit(gauss, *part(blizko, *peak).T, maxfev=10000)

param_daleko_bot, sp = optimize.curve_fit(gauss, *kraje_daleko.T, maxfev=10000)
param_daleko_peak, sp = optimize.curve_fit(gauss, *part(daleko, *peak).T, maxfev=10000)

print ("koeficienty fitu")

print(param_blizko_bot)
print(param_blizko_peak)
print(param_daleko_bot)
print(param_daleko_peak)


plt.plot(part(x_fity, *dolni), gauss(part(x_fity, *dolni),*param_blizko_bot), "b")
plt.plot(part(x_fity, *horni), gauss(part(x_fity, *horni),*param_blizko_bot), "b")
plt.plot(part(x_fity, *peak), gauss(part(x_fity, *peak),*param_blizko_peak), "b")


plt.plot(part(x_fity, *dolni), gauss(part(x_fity, *dolni),*param_daleko_bot), "g")
plt.plot(part(x_fity, *horni), gauss(part(x_fity, *horni),*param_daleko_bot), "g")
plt.plot(part(x_fity, *peak), gauss(part(x_fity, *peak),*param_daleko_peak), "g")



print("Blizko: ")

maximal_blizko = gauss(param_blizko_peak[0], *param_blizko_peak)
bord = gauss(param_blizko_peak[0], *param_blizko_peak)/20

fgauss = lambda x: gauss(x, *param_blizko_bot)-bord

print(blizko[:,0])
print (*border(blizko[:,0], *dolni))

roots_blizko = optimize.brentq(fgauss, *border(blizko[:,0], *dolni)), optimize.brentq(fgauss, *border(blizko[:,0], *horni))

print("maximalni hodnota: " + str(max) + "5% ~ " + str(bord))
print("uhel vyzarovani: "+  str(roots_blizko[0]-roots_blizko[1]))

theta = (-roots_blizko[0]+roots_blizko[1])/2

lmbda = 632

print("Daleko: ")

maximal_daleko = gauss(param_daleko_peak[0], *param_daleko_peak)
bord_daleko = gauss(param_daleko_peak[0], *param_daleko_peak)/20

fgauss = lambda x: gauss(x, *param_daleko_bot)-bord_daleko

print (fgauss(border(daleko[:,0], *dolni)[0]))
print (fgauss(border(daleko[:,0], *dolni)[1]))

roots_daleko = optimize.brentq(fgauss, *border(daleko[:,0], *dolni)), optimize.brentq(fgauss, *border(daleko[:,0], *horni))

#print("maximalni hodnota: " + str(maximal) + "5% ~ " + str(bord_daleko))
print("uhel vyzarovani: "+  str(roots_daleko[0]-roots_daleko[1]))

a=980e3

NA = math.sin(theta)
V = (2*np.pi/lmbda)*a*NA
print("NA = " + str(NA))
print("V = " + str(V))


ax=plt.axes()

ax.arrow(roots_blizko[0], maximal_blizko, 0, bord-maximal_blizko, fc="b", ec="b", length_includes_head = True)
ax.arrow(roots_blizko[1], maximal_blizko, 0, bord-maximal_blizko, fc="b", ec="b", length_includes_head = True)

ax.arrow(roots_daleko[0], maximal_daleko, 0, bord_daleko-maximal_daleko, fc="g", ec="g", length_includes_head = True)
ax.arrow(roots_daleko[1], maximal_daleko, 0, bord_daleko-maximal_daleko, fc="g", ec="g", length_includes_head = True)

plt.ylabel("p [mW]")
plt.xlabel("uhel [°]")
plt.legend()


plt.show()