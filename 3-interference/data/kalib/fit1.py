import numpy as np
import scipy.optimize as opt
import scipy
import scipy.stats
from imageio import imread
import matplotlib.pyplot as plt
import math
from subprocess import call
from random import random


start = 250
rowNum = 80

lmbda = 632.8

def shift(num):
	print("evaluating for distance: " + str(num))
	#command = 'ffmpeg -i "' + str(num) + '.mpg" ./raw_images/' +str(num)+'_img-%03d.bmp'
	#call([command], shell=True)



	image = imread("./raw_images/" +str(num)+"_img-030.bmp")[:,20:-160,0]
	data=image[start:start+rowNum,:]
	m, n = data.shape[0:2]

	x=range(n)

	y = np.zeros(n)
	for i in range(n):
		y[i]=data[:,i].mean()



	def intenzita(x, a, b, c, omega, phi):
		return a*np.sin(omega*x+phi)+b*x+c


	initial = list((8.33574165e+01,-5.28221557e-02,3.87758270e+01,4.71511185e-02,0))


	bounds=((-0, -np.inf, -np.inf, 1e-2, -np.inf), (+np.inf, +np.inf, +np.inf, 1.5e-1, +np.inf))


	(args, cor) = opt.curve_fit(intenzita, x, y, initial, maxfev = 100000, bounds = bounds)
	devs = cor.diagonal()
	dev_phi = devs[4]


	plt.clf()
	plt.imshow(data)
	plt.plot(x, y, color="magenta")
	plt.plot(x, intenzita(x, *args), color="lime")
	plt.ylabel("Není na škále")
	plt.xlabel("dx")
	plt.title("U = " + str(num) + "V")


	#plt.show(block=False)
		#just pray

	plt.savefig("shift_"+str(num).replace(".", "_")+".pdf", bbox_inches='tight')

	n = args[4]/(2*np.pi)
	d= (lmbda*n)/2

	return(d)



rozsah = (2.7, 5.2, 6.1, 7.1, 8.2, 8.7, 9.3)

shift_d = np.zeros_like(rozsah)

for i in range(len(rozsah)):
	shift_d[i]=shift(rozsah[i])



k, q, a, b, c = scipy.stats.linregress(rozsah, shift_d)
x_fit = np.arange(rozsah[0], rozsah[-1], 0.01)


plt.clf()
plt.plot(rozsah, shift_d, 'x')
plt.plot(x_fit, k*x_fit+q)

plt.xlabel("U [V]")
plt.ylabel("d [nm]")


print(k)

plt.show()


import pdb
pdb.set_trace()