import numpy as np
import scipy.optimize as opt
from imageio import imread
import matplotlib.pyplot as plt
import math
from subprocess import call


start = 205
rowNum = 20

def visibility(num):
	print("evaluating for distance: " + str(num))
	command = 'ffmpeg -i "' + str(num) + '.mpg" ' +str(num)+'_img-%03d.bmp'
	call(command)




	image = imread(str(num)+"_img_030.bmp")[:,20:-20,0]
	data=image[start:start+rowNum,:]
	m, n = data.shape[0:2]

	x=range(n)

	y = np.zeros(n)
	for i in range(n):
		y[i]=data[:,i].mean()



	def intenzita(x, a, b, c, omega, phi):
		return a*np.sin(omega*x+phi)+b*x+c


	initial = (4.33574165e+01,-5.28221557e-02,9.87758270e+01,4.71511185e-02,1.82977992e+00)
	bounds=((-0, -np.inf, -np.inf, 2e-2, -np.inf), (+np.inf, +np.inf, +np.inf, 9e-2, +np.inf))

	(args, cor) = opt.curve_fit(intenzita, x, y, initial, maxfev = 100000, bounds = bounds)
	#args = initial

	z = np.zeros

	dif = 2*args[0]
	add = 2*(args[2]+args[1]*n/2)
	v=dif/add

	print(args)

	print (v)

	plt.imshow(data)
	plt.plot(x, y)
	plt.plot(x, intenzita(x, *args))
	plt.show(block=False)


dl = np.zeros(15)
v = np.zeros(15)


j=0
for num in range(810, 650, -10):
	dl[j] = 10*j
	v[j] = visibility (j)
	j+=1
	pass


plt.show()