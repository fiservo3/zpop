import numpy as np
import scipy.optimize as opt
import scipy
from imageio import imread
import matplotlib.pyplot as plt
import math
from subprocess import call
from random import random


start = 250
rowNum = 80

def visibility(num):
	print("evaluating for distance: " + str(num))
	command = 'ffmpeg -i "' + str(num) + '.mpg" ./raw_images/' +str(num)+'_img-%03d.bmp'
	call([command], shell=True)




	image = imread("./raw_images/" +str(num)+"_img-030.bmp")[:,20:-150,0]
	data=image[start:start+rowNum,:]
	m, n = data.shape[0:2]

	x=range(n)

	y = np.zeros(n)
	for i in range(n):
		y[i]=data[:,i].mean()



	def intenzita(x, a, b, c, omega, phi):
		return a*np.sin(omega*x+phi)+b*x+c


	initial = list((3.00603775e+01, -6.61016225e-02,   9.85452941e+01,   5.67373529e-02, 8.16537952e-01))


	bounds=((1e01, -np.inf, -np.inf, 1e-2, -np.inf), (+np.inf, +np.inf, +np.inf, 1.5e-1, +np.inf))


	(args, cor) = opt.curve_fit(intenzita, x, y, initial, maxfev = 100000)
	#args=initial
	devs = cor.diagonal()
	dev_phi = devs[4]


	z = np.zeros

	dif = 2*args[0]
	add = 2*(args[2]+args[1]*n/2)
	v=dif/add

	print(args)

	print (v)

	plt.clf()
	plt.imshow(data)
	plt.plot(x, y)
	plt.plot(x, intenzita(x, *args))
	plt.show(block=True)
	#plt.savefig("visib_"+str(num)+".pdf", bbox_inches='tight')

	return(v, abs(devs[4])*abs(devs[3]))


dl = np.zeros(16)
v = np.zeros(16)
devs = np.zeros(16)


j=0
for num in range(660, 810, 10):
	dl[j] = 10*j
	v[j], devs[j] = visibility(num)
	j+=1



plt.clf()

plt.plot(dl, v)
plt.plot(dl, devs)

print(dl)
print(v)

#plt.show()