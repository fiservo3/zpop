import numpy as np
import scipy.optimize as opt
import scipy
from imageio import imread
import matplotlib.pyplot as plt
import math
from subprocess import call
from random import random


start = 250
rowNum = 80

def visibility(num):
	print("evaluating for distance: " + str(num))
	command = 'ffmpeg -i "' + str(num) + '.mpg" ./raw_images/' +str(num)+'_img-%03d.bmp'
	call([command], shell=True)




	image = imread("./raw_images/" +str(num)+"_img-030.bmp")[:,20:-160,0]
	data=image[start:start+rowNum,:]
	m, n = data.shape[0:2]

	x=range(n)

	y = np.zeros(n)
	for i in range(n):
		y[i]=data[:,i].mean()



	def intenzita(x, a, b, c, omega, phi):
		return a*np.sin(omega*x+phi)+b*x+c


	initial = list((8.33574165e+01,-5.28221557e-02,3.87758270e+01,4.71511185e-02,0))


	bounds=((-0, -np.inf, -np.inf, 1e-2, -np.inf), (+np.inf, +np.inf, +np.inf, 1.5e-1, +np.inf))


	(args, cor) = opt.curve_fit(intenzita, x, y, initial, maxfev = 100000, bounds = bounds)
	devs = cor.diagonal()
	dev_phi = devs[4]

	doubt = False
	while dev_phi >= 0.02:
		initial[4] = random()
		initial[3] = random()*7e01

		(args, cor) = opt.curve_fit(intenzita, x, y, initial, maxfev = 100000)
		devs = cor.diagonal()
		dev_phi = devs[4]

		print(dev_phi)
		doubt=True




	z = np.zeros

	dif = 2*abs(args[0])
	add = 2*(abs(args[2])+args[1]*n/2)
	v=dif/add

	#print(args)

	print (v)
	dl = 2*(810-num +250-260)
	plt.clf()
	plt.imshow(data)
	plt.plot(x, y)
	plt.plot(x, intenzita(x, *args))
	plt.ylabel("Není na škále")
	plt.xlabel("dx")
	plt.title("dl = " + str(dl))
	if doubt:
		#plt.show()
		#just pray
		pass
	plt.savefig("visib_"+str(num)+".pdf", bbox_inches='tight')

	return(v, dev_phi)


dl = np.zeros(16)
v = np.zeros(16)
devs = np.zeros(16)


j=0
for num in range(810, 650, -10):
	dl[j] = 2*(810-num +250-260)
	v[j], devs[j] = visibility(num)
	j+=1

plt.clf()


p=np.polyfit(dl, v, 8)

x_fit = np.arange(dl[0], dl[-1], 0.01)

pos_visib = lambda x: np.polyval(p, x) - np.polyval(p, 0) /2
sirka = opt.brentq(pos_visib, 0, 160)

print("delka=" + str(sirka))

plt.plot(dl, v, "rx", label = "Spočtená visbilita")
plt.plot(x_fit, np.polyval(p, x_fit), label = "Polynomiální fit" )
#plt.plot(x_fit, pos_visib(x_fit), label = "Polynomiální fit" )
plt.arrow(sirka, np.polyval(p, 0), 0, - np.polyval(p, dl[0]) /2, fc="b", ec="b", length_includes_head = True, label="pokles na polovinu")



print(dl)
print(v)


plt.xlabel("dl [cm]")
plt.ylabel("Visibilita []")

plt.legend()
plt.show()

import pdb
pdb.set_trace()