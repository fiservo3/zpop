import numpy as np

def deg(deg, mi, vt):
	return deg+(1/60)*mi+(1/3600)*vt


phi1 =np.array((38, 00, 40))
phi2 =np.array((245, 54, 58))

phi_deg = (deg(*phi2)-deg(*phi1)) - 180
phi_deg_2 = phi2-phi1 


print("lamavy uhel: " +str(phi_deg) + "°")




phi = phi_deg*2*np.pi/360
print("lamavy uhel: " + str(phi) + " rad")

dev_a = (20, 52, 48)
dev_b = (351, 4, 24)


delta_min_deg=(360-deg(*dev_b)+deg(*dev_a))/2


delta_min = delta_min_deg*2*np.pi/360

print("delta = " + str(delta_min_deg))
print("2 delta = " + str(delta_min_deg*2))

nh = (np.sin((delta_min+phi)/2)/np.sin(0.5*phi)).mean()
sigma_nh = (np.sin((delta_min+phi)/2)/np.sin(0.5*phi)).std()
print("nh= " + str(nh) + " +- " + str(sigma_nh))