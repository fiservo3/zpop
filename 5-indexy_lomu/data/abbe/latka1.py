import numpy as np

nh=1.3

alpha=np.array([1,2])


nv = (np.sin(alpha*2*np.pi/360)*nh).mean()
sigma_nv = (np.sin(alpha*2*np.pi/360)*nh).std()
print("nv= " + str(nv) + " +- " + str(sigma_nv))